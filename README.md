### What is this repository for? ###

Repository created for the second lesson of modern react with redux course at Udemy. 
Lessons by Stephen Grider - Engineering Architect (https://github.com/StephenGrider)

### How do I get set up? ###

* npm install
* npm start

